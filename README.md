# additional_import

Modules and projects to explore the functionality of python's import statement.

## Environment
To successully run this code, please run in the docker container:

```
docker run -d \
-p 8888:8888 \
-e GRANT_SUDO=yes \
--user root \
-v ${PWD}:/home/jovyan/work \
jupyter/tensorflow-notebook
```

To get the token or url to the jupyter notebook, you can check the docker logs:

```
docker logs <name_of_container>
```

Install a new dependency in your container:

```
docker exec -it <name_of_container> bash
apt-get update
apt-get install tree
exit
```